#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  ejer_calcu.py
#  
#  Copyright 2019 Marcia <Marcia@DESKTOP-KLOML7G>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  
#  
import calculadora

def main(args):
    print(calculadora.suma(3,2))
    print(calculadora.resta(2,2))
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
