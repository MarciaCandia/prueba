import os
os.system('cls')
#----------------------------------------------------
print('\n\nestamos aprendiendo')
def mensaje(): #declaración de la funcion
	print('funcion de prueba')

mensaje()

#---------SUMA--------------
def suma():
 	n1=5
 	n2=6
 	print(n1+n2)
#---------------------------
suma()
#---------------------------
#---------------------------
def suma_args(n1,n2):
	print(n1+n2)
#--------------------------
suma_args(5,25)
#--------------------------
#--------------------------
def suma_return(n1,n2):
	variable=(n1+n2)

	return variable,"Con return"
#Python pasa siempre los valores por referencia
#--------------------------
print(suma_return(5,8))
#--------------------------
#-----------------------------LISTAS----------------------------
#lAS listas en PY nos permite guardar diferentes tipos de valores y se pueden expandir dinámicamente
#el primer índice siempre es cero
tres='tres'
cuatro='cuatro'
L1=[1,2,tres,cuatro,"carolina"]
print(L1)
print(L1[2])
print(L1[2:])
print(L1[:4])
print(L1[:])
print(L1[-1])
print(L1[0:2])#no se incluye el contenido del índice 2. Primero incluído segundo excluído

L1.append('sandra') #agrega elemento al final
print(L1[:])

L1.insert(4,'peperula')
print(L1[:])

L1.extend(['corazon','de','papel'])
print(L1[:])

print('indice de corazon =',L1.index('corazon'))

print('pepe' in L1)#imprime falso porque no está pepe, retorna booleanos

L1.remove('corazon')
print(L1[:])

L1.pop()#elimina el ultimo elemento de la lista
print(L1[:])

L2=[2,3,4,5,7,8]

L3=L1+L2  # el + funciona como concatenador de listas
print(L3)

L3=L1*3
# el * funciona a modo de repetidor
print(L1)
print('\n L1 con repetidor',L3)

#help()
#print('exit()')
#clear_all()
#cls()
#clear_output

#-------------------------------------------LAS TUPLAS--------------------------------------------------------------
# SON LISTAS INMUTABLES, NO PUEDEN CAMBIAR, NO SE PUEDEN AÑADIR, ELIMINAR, 
# NI MODIFICAR ELEMENTOS, se usan Parantesis, se permite el índice
tupla1=(1,'j',346,'cabeza',5,6,4,3)
print(tupla1)
milista=list(tupla1)#convierte la tupla en lista
print(milista)
tupla2=tuple(milista)
print(tupla2)
print(tupla2.count('cabeza'))#cuanta cuantas veces esta cabeza
print(len(tupla2))#cuenta cuantos elementos tiene

#DESEMPAQUETADO DE TUPLA
tupla=('juan',13,1,1987)
nombre,dia,mes,ano=tupla
print(nombre)
print(dia)
print(mes)
print(ano)
#-----------------------------------------------------------------------------------------------------------------

#-----------------------------------DICCIONARIO---------------------------------------------------------------------
#--------------------------------CLAVE(única):VALOR---------------------------------------------------
capitales={"Alemania":"Berlin","Francia":"París","Inglaterra":"Londres","España":"Madrid"}#paises y capitales#va entre llaves
print(capitales["Alemania"])
print(capitales)
#agregar elemento
capitales["Italia"]="Lisboa"
print(capitales)

#modificar un valor
capitales["Italia"]="ROMA"
print(capitales)

#eliminar
del capitales["Inglaterra"]
print(capitales)

#usando la tupla para asignar las claves
mitupla=["uno","dos","alemania"]
capitales={mitupla[0]:"Berlin",mitupla[1]:"París","mitupla[2]":"Londres","España":"Madrid"}
print(capitales)

#almacenar tupla entera
diccionario={23:"jordan","nombre":"michaesl","anillos":[1199,195,1558]}
print(diccionario)

#diccionario dentro de diccionario#
diccionario={23:"jordan","nombre":"michaesl","anillos":{"temporadas":[1199,195,1558]}}
print(diccionario)
print(diccionario["anillos"])

print(diccionario.keys())#claves
print(diccionario.values())#valores
print(len (diccionario))# cantidad de claves
#

#------------------------------------ESTRUCTURAS DE CONTROL DE FLUJOS---------------------
print("intento de borrado")
os.system('cls')
#------------------------------------CONDICIONALES Y BUCLES-------------------------------
print("\n \nProgramita de evaluación")

#nota_alumno=input("Introduce la Nota:_") #hay que abrir la consola para introducir datos
#cualquier cosa que metamos con input() es considerado como texto
#la infuncion input puede admitir parametros "introduce la nota"
#se introduce la nota a traves de la consola RRPL python Run current file
#nota_alumno=int(nota_alumno)#lo transformo a entero 

nota_alumno=5
def evaluacion(nota):
	valoracion="aprobado"
	if nota <= 5 : # si ambos no son enteros da error
		valoracion="suspenso"
	return valoracion


print(evaluacion(nota_alumno),'\n')
#introducir la nota por teclado:

#--------------------------------------------------------------------------------------------
print("verificacion de acceso")
edad=int(input("Introduce tu Edad_"))

"""if edad < 18 :
	print("no puedes pasar") 

if edad > 100 :
		print("Edad Incorrecta")
else:
	print("SI puede pasar") """ #Corregimos con ELIF (else if en C)
#--------------------------------------------------------------------------------------------
if edad < 18 :
	print("no puedes pasar") 

elif edad > 100 :
		print("Edad Incorrecta")
else:##entra en el else cuando nada de lo anterior se ha cumplido
	print("SI puede pasar")
#--------------------------------------------------------------------------------------------
edad=int(input("Introduce tu NOTA_"))

if edad < 5 :
	print("INSUFICIENTE") 

elif edad < 6 :
	print("SUFICIENTE")

elif edad < 7 :
	print("BIEN")

elif edad < 9 :
	print("NOTABLE")

else:##entra en el else cuando nada de lo anterior se ha cumplido, SOLO EL ELSE CIERRA EL IF
	print("EXCELENTE")
#-----------------------------------------------------------------------------------------------